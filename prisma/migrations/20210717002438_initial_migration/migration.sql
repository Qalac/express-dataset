-- CreateTable
CREATE TABLE "Actor" (
    "id" SERIAL NOT NULL,
    "login" TEXT NOT NULL,
    "avatar_url" TEXT,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Repo" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "repo_url" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Event" (
    "id" SERIAL NOT NULL,
    "type" TEXT NOT NULL,
    "actorId" INTEGER NOT NULL,
    "repoId" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Actor.login_unique" ON "Actor"("login");

-- CreateIndex
CREATE UNIQUE INDEX "Repo.name_unique" ON "Repo"("name");

-- AddForeignKey
ALTER TABLE "Event" ADD FOREIGN KEY ("actorId") REFERENCES "Actor"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD FOREIGN KEY ("repoId") REFERENCES "Repo"("id") ON DELETE CASCADE ON UPDATE CASCADE;
