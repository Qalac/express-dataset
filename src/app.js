var express = require('express');
var bodyParser = require('body-parser');
var Actor = require('./routes/actor');
var Events = require('./routes/events');
var dotenv = require('dotenv');

dotenv.config();

var app = express();
var port = process.env.PORT

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/', Actor)
app.use('/', Events)

app.listen(port, () => {
    console.log(`The application is listening on port ${port} 🚀 `);
})
