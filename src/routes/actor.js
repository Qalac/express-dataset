var express = require('express');
var {updateAvatar, getActorByStreak, getAllActors} = require('../controllers/actors');

var router = express.Router();

router.put('/actors', updateAvatar);
router.get('/actors', getAllActors);
router.get('/actors/streak', getActorByStreak);


module.exports = router;