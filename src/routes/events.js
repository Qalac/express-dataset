var express = require('express');
var {eraseEvents, eventsRecord, createEvent, getAllEvents} = require('../controllers/events');

var router = express.Router()

router.delete('/erase', eraseEvents);
router.post('/events', createEvent);
router.get('/events', getAllEvents);
router.get('/events/actors/:id', eventsRecord)

module.exports = router;