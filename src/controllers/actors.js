var {PrismaClient} = require('@prisma/client')

const prisma = new PrismaClient()


var updateAvatar = async (req, res) => {
	const {id} = req.params

	var getActor = await prisma.actor.findUnique({
		where: {id: Number(id)},
	})
	
	if(!getActor) {
		res.status(404);
	}
	else {
		var updatedActor = await prisma.actor.update({
			where: {id: Number(id)},
			data: { ...req.body}
		})
		res.send(updatedActor)
		res.status(200)
	}
}

var getAllActors = async (req, res) => {
	var allActors = await prisma.actor.findMany({
		orderBy: {
			OR: [
				{
					events: {
						count: 'desc'
					}
				},
				{
					events: {
						created_at: 'desc'
					}
				},
				{
					login: 'asc'
				}
			]
		},
		include: {events: true}
	})
	if (allActors) {
		res.send(allActors)
		res.status(200)
	}
	else {
		res.status(404)
	}
}


var getActorByStreak = async (req, res) => {
	var getActors = prisma.actor.findMany({
		orderBy: {
			OR: [
				{
					events: {
						count: 'desc'
					}
				},
				{
					events: {
						created_at: 'desc'
					}
				},
				{
					login: 'asc'
				}
			]
		},
		include: {events: true}
	})
	if (getActors) {
		res.send(getActors)
		res.status(200)
	}
	else {
		res.status(404)
	}
}

module.exports = {
	getActorByStreak: getActorByStreak,
	getAllActors: getAllActors,
	updateAvatar: updateAvatar
}