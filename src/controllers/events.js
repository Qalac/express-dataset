var {PrismaClient} = require('@prisma/client');

const prisma = new PrismaClient()


var eraseEvents = async (req, res) => {
	var result = await prisma.event.deleteMany();
	res.status(200);
}

var createEvent = async (req, res) => {
	const newEvent = await prisma.event.create({
		data: {
			 ...req.body,
			 actor: {
				 connect: {login: req.body.login}
			},
			repo: {
				connect: {name: req.body.name}
			}
		},
	})
	const eventSearch = prisma.event.findUnique({
		where: {id: newEvent.id}
	})
	if(!eventSearch) {
		res.send(newEvent)
		res.status(201)
	}
	else {
		res.status(400)
	}
}

var getAllEvents = async (req, res) => {
	var allEvents = await prisma.event.findMany({
		orderBy: {id: 'asc'},
		include: { actor: true, repo: true }
	});
	if (allEvents) {
		res.send(allEvents);
		res.status(200);
	}
	else {
		res.status(404);
	}

}

var eventsRecord = async (req, res) => {
	let {id} = req.params
	var events = await prisma.event.findMany({
		where: {
			actorId: Number(id)
		},
		orderBy: {id: 'asc'},
		include: { actor: true, repo: true }
	})
	if(events) {
		res.send(events);
		res.status(200)
	}
	else {
		res.status(404)
	}
}

module.exports = {
	eraseEvents: eraseEvents,
	createEvent: createEvent,
	getAllEvents: getAllEvents,
	eventsRecord: eventsRecord
}